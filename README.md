# Testing Markdown
---
## This is the second header
I am trying to figure out how markdown works. This is the first time that I use commands and programming languages. It is super cool. This is how it looks like if I do not put a empty line after I have written something.
#
I am trying to figure out how markdown works. 

This is the first time that I use commands and programming languages.

It is super cool. 

This is how it looks like if I **put** a empty line after I have written something

---
## Playing around with Media
![flower](https://upload.wikimedia.org/wikipedia/commons/d/d3/Nelumno_nucifera_open_flower_-_botanic_garden_adelaide2.jpg "flower")

[This](https://upload.wikimedia.org/wikipedia/commons/d/d3/Nelumno_nucifera_open_flower_-_botanic_garden_adelaide2.jpg "flower") is how the flower would look like by using a Hyperlink 

---
## Header
### Header
#### Header
##### Header
###### Header

---
## Testing Lists
| 1 	| 2 	| 3 	|
|----	|----	|----	|
| a 	| b 	| c 	|
| a1 	| b1 	| c1 	|

## Ingredients needed to make a cake:
1. **Flour**

    * white

    * dark

    * Wheat

        1. *bleached*

        2. *unbleached*

    * Maiz


2. **Milk**

3. **Eggs**

4. **Sugar**

5. **Butter**



# MarkDown Basics

A simple repository with a README.md file to test the effects of
MarkDown formatting.

Test how this document changes when you change the formatting or
content:
1. Fork this repository
2. Edit the README.md file
3. Commit the changes
4. Check the result

Have fun!